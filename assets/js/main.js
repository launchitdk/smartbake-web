$( document ).ready(function() {
  // Toggles "is-on" on both nav-burger icon and actual-nav, when the nav-burger is clicked
var scrollPos = 0;

  $(".nav__burger").on("click", function(event){
    event.preventDefault();
    var willShow = !$(this).hasClass("is-on");

    if (willShow) {
      scrollPos = $(window).scrollTop();
    }

    $(this).toggleClass("is-on");
    $(".actual-nav").toggleClass("is-on");
    // Disables scroll while .actual-nav is open
    $("html, body").toggleClass("is-on");

    if (!willShow) {
      window.scrollTo(0, scrollPos);
      prev = scrollPos;
    }
  });


  // Hides/shows nav on scroll
  var prev = 0;

  $(window).on("scroll", function(){
    var scrollTop = $(window).scrollTop();
    $("nav").toggleClass("hidden", scrollTop > prev);
    prev = scrollTop;

    // Adds midnight theme when nav is scrolled 100px
    if (scrollTop >= 100) {
        // console.log("More than 100px");
        $("nav").addClass("nav--midnight");
    } else {
        // console.log("Less than 100px");
        $("nav").removeClass("nav--midnight");
        $("nav").removeClass("hidden");
    }
  });
});

//Cookie handling
$(document).ready(function() {

	var cookieAcceptKey = "cookies=i-accept-ofc";

	$('.cookie__btn').on('click', function(e) {
		e.preventDefault();

		var expirationDate = new Date();
		expirationDate.setYear(expirationDate.getFullYear() + 2);
		document.cookie = cookieAcceptKey + "; expires=" + expirationDate;

		$('.cookie').addClass('hidden');
	});

	if (document.cookie == "") {
		$('.cookie').removeClass('hidden');
	}
});
